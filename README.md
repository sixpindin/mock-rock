# README #

### What is this repository for? ###

Mock-Rock is an HTTP proxy that allows you to 'mock out' endpoints. It may be used for things like endpoints which don't exist yet, but for-which you have the specification; allowing you to proceed with development as normal, as if the endpoint existed.

Currently a proof-of-concept.

### How do I get set up? ###

1. Install node JS
2. cc to source dir
3. node --harmony server/js


### Who do I talk to? ###

* sixpindin@gmail.com