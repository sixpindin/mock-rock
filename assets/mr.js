(function(){
    console.log('started Mr...');

function httpGet(theUrl)
{
    var xmlHttp = null;

    xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", theUrl, false );
    xmlHttp.send( null );
    return xmlHttp.responseText;
}

function getIntercepts(){
    var intercepts = httpGet('/intercepts');
    return intercepts;
}

    function getListItemsHtml(json){
	json = JSON.parse(json);
    if(json.count < 1){
	return [];
    }
	var ret = [];
	json.forEach(function(item){
            var cssClass = (item.indexOf('Proxied:') !== -1) ? 'proxied' : 'intercepted';
	ret.push('<li class="' + cssClass + '">' + item  +  '</li>');
    });
    return ret;
};

function getStartedState(){
  var state = httpGet('/startedState');
  return state;
}

//update intercepts list
setInterval(function(){
    var intercepts = getIntercepts();
    if(intercepts.length == 0) return;
    var requestsList = $('#requests');
    requestsList.empty();
    requestsList.append(getListItemsHtml(intercepts));
    $('#statusDiv').scrollTop(parseInt(requestsList.css('height')));
}, 1000);

//update started/stopped state
setInterval(function(){
  var state = getStartedState();
  if(state === 'true'){
    console.log('Started...');
    $('#status').text('Status: Started');
  }else{
    console.log('Stopped...');
    $('#status').text('Status: Stopped');
}
},1000);


})();
