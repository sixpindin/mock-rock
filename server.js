var hoxy = require('hoxy');
var store = require('./store.js');
var port = 8000;
var proxy = new hoxy.Proxy().listen(port, function(){
  console.log('The proxy is listening on port ' + port + '.');
});

var intercepts_stack = [];
var startedState = false;

function updateResponseOnIgnoredIntercept(res){
  console.log(JSON.stringify(res));
}

function loadProxyIntercepts(){
proxy.intercept({
    phase: 'request',
    as: 'string'
}, function(req,resp){
    if(intercepts_stack[0].indexOf('Waiting') !== -1){
	intercepts_stack.splice(0,1);
    }
    console.log('intercepted:' + req.fullUrl());
    var rules = store.getRules();
    console.log('response:\n' + JSON.stringify(resp));

    for (var num = 0; num < rules.length; num++) {
	var rule = rules[num];
	console.log('testing rule:\n' + JSON.stringify(rule));
	console.log('rule url is:' + rule.url);
	if(rule.url === req.fullUrl()){
	    resp.string = rule.response;
	    intercepts_stack.push('Intercepted: ' + req.fullUrl());
	    return;
	}
    }

    //if we're here then the request was proxied as normal
    intercepts_stack.push('Proxied: ' + req.fullUrl());
    updateResponseOnIgnoredIntercept(res);
});
}



function clearProxyIntercepts(){
    proxy.clearIntercepts();
}

////
////KOA
////
var views = require("co-views");
var koa = require('koa');
var app = module.exports = koa();
var render = views("./views/", { map: { html: 'swig' }});
var fs = require('fs');
var parseBody = require('co-body');
//
//normal pipeline stuff
//


//
//http routed stuff
//

//[get:/]
app.use(function *(next){
    if(this.path !== '/') return yield next;
    this.body = yield render("mr");
});

//[get:assets/*]
app.use(function *(next){
    if(this.path.indexOf('/assets') == -1) return yield next;
    this.body=fs.readFileSync('.' + this.path);
});

//[get /intercepts]
app.use(function *(next){
    if(this.path !== '/intercepts') return yield next;
    this.body=intercepts_stack;
});

//[post /start]
app.use(function *(next){
    if(this.path !== '/start') return yield next;
    console.log("Processing start request.");
    loadProxyIntercepts();
    startedState = true;
   intercepts_stack.push('Waiting for requests...');
  this.redirect('/');
});

//[post /stop]
app.use(function *(next){
    if(this.path !== '/stop') return yield next;
    clearProxyIntercepts();
    startedState=false;
    this.redirect('/');
});

//[get /startedState]
app.use(function *(next){
    if(this.path !== '/startedState') return yield next;
    this.body = startedState;
});

//[put /rule]
app.use(function *(next){
    if(this.path !== '/rule' || this.method !=='PUT') return yield next;
    var body = yield parseBody(this);
    console.log('request to store rule: \n' + body);
    store.putRule(body);
    this.redirect('/');
});

app.listen(3000);
